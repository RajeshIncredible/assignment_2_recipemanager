package com.mgr;




public class Recipe {
    private int recipeID;
    private String recipeName;
    private String ingrediant_1;
    private String ingrediant_2;
    private String ingrediant_3;
    private String ingrediant_4;
    
	public Recipe(int recipeID, String recipeName, String ingrediant_1, String ingrediant_2, String ingrediant_3,
			String ingrediant_4) {
		super();
		this.recipeID = recipeID;
		this.recipeName = recipeName;
		this.ingrediant_1 = ingrediant_1;
		this.ingrediant_2 = ingrediant_2;
		this.ingrediant_3 = ingrediant_3;
		this.ingrediant_4 = ingrediant_4;
	}
    
	public int getRecipeID() {
		return recipeID;
	}
	public void setRecipeID(int recipeID) {
		this.recipeID = recipeID;
	}
	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	public String getIngrediant_1() {
		return ingrediant_1;
	}
	public void setIngrediant_1(String ingrediant_1) {
		this.ingrediant_1 = ingrediant_1;
	}
	public String getIngrediant_2() {
		return ingrediant_2;
	}
	public void setIngrediant_2(String ingrediant_2) {
		this.ingrediant_2 = ingrediant_2;
	}


	public String getIngrediant_3() {
		return ingrediant_3;
	}
	public void setIngrediant_3(String ingrediant_3) {
		this.ingrediant_3 = ingrediant_3;
	}
	public String getIngrediant_4() {
		return ingrediant_4;
	}

	public void setIngrediant_4(String ingrediant_4) {
		this.ingrediant_4 = ingrediant_4;
	}
	
	@Override
	public String toString() {
		return "Recipe [recipeID=" + recipeID + ", recipeName=" + recipeName + ", ingrediant_1=" + ingrediant_1
				+ ", ingrediant_2=" + ingrediant_2 + ", ingrediant_3=" + ingrediant_3 + ", ingrediant_4=" + ingrediant_4
				+ "]";
	}
	

    	
}
