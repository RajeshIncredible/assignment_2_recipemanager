package com.mgr;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.mgr.Recipe.*;



import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WelcomeController {
	
	RecipeUtils RC = new RecipeUtils();
	
	// inject via application.properties
	@Value("${welcome.message:test}")
	private String message = "Hello World";
	
   RecipeUtils RU;
   List<Recipe> tempsaveRecipe = new ArrayList<Recipe>();
   List<Recipe> tempselectedRecipe = new ArrayList<Recipe>();
	@RequestMapping("/")
	public String welcome(Map<String, Object> model) {
		model.put("message", this.message);

		return "welcome";
	}
	
	@RequestMapping(value= {"/getRecipeList"}, headers="Accept=application/json")
	public @ResponseBody String getRecipeList() {
		System.out.println("Inside the get Recipe List");
		/*String recipeList = RU.getSampleRecipe().toString();*/
		
		
		Gson gson = new Gson();
		String jsonRecipe = gson.toJson(RU.getSampleRecipe());
		System.out.println("gson converted Recipe List------------->"+jsonRecipe);
		
		return jsonRecipe;
	}
	
	@RequestMapping(value= {"/recipesave"}, headers="Accept=application/json")
	public @ResponseBody Boolean saveRecipe(@RequestBody  Recipe rc, BindingResult result, HttpServletRequest request) {
		
		Boolean retString = null;
		/*String recipeList = RU.getSampleRecipe().toString();*/
		
		int id = rc.getRecipeID();
		String Name = rc.getRecipeName().replace(" ", "");
		String ingr1 = rc.getIngrediant_1();
		String ingr2 = rc.getIngrediant_2();
		String ingr3 = rc.getIngrediant_3();
		String ingr4 = rc.getIngrediant_4();
		
		for (Recipe r : RC.getSampleRecipe()) {
			
			
			if (Name.equalsIgnoreCase(r.getRecipeName().replace(" ", ""))) {
				
				retString = true;
				break;
			}
			else  {
				tempsaveRecipe.add(new Recipe(id,Name,ingr1,ingr2,ingr3,ingr4));
				retString =  false;	
			}
		}
		
		return retString;
		
		
		
	}
	
	@RequestMapping(value= {"/getSelectedRecipeList"}, headers="Accept=application/json")
	public @ResponseBody String selectedRecipeList(@RequestBody Recipe rc, BindingResult result, HttpServletRequest request) {
		System.out.println("Inside the get selected Recipe List");
		/*String recipeList = RU.getSampleRecipe().toString();*/
		int selectedRecipeID = rc.getRecipeID();
		System.out.println("selectedRecipeList ---->"+rc.getRecipeID());
		for (Recipe r : RU.getSampleRecipe()) {
		if (selectedRecipeID == r.getRecipeID()) {
			tempselectedRecipe.add(r);
			System.out.println("Selected Recipe below is the details");
			System.out.println(tempselectedRecipe.toString());
		}
		}
		Gson gson = new Gson();
		String jsonselectedRecipe = gson.toJson(tempselectedRecipe);
		System.out.println("gson converted Recipe List------------->"+tempselectedRecipe.toString());
		
		return jsonselectedRecipe;
	}
}