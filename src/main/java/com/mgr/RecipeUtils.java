package com.mgr;


import java.util.ArrayList;

import java.util.List;


public class RecipeUtils {

    public static List<Recipe> getSampleRecipe() {
        List<Recipe> rc = new ArrayList<Recipe>();

        rc.add(new Recipe(1, "Masala Bhindi", "Bhindi", "Cumin Seeds", "Fennel Seeds", "Onion"));
        rc.add(new Recipe(2, "Chana Kulcha",  "cumin powder", "ginger", " coriander powder", "carom powder"));
        rc.add(new Recipe(3, "Sahi Egg Cury","kasurimethi", "fresh cream", "yogurt", "fresh coriander"  ));
        rc.add(new Recipe(4, "Gujraji Kadhi", "yogurt", "gram flour", "curry leaves", "ginger" ));
        rc.add(new Recipe(5, "Allahabad Ki Tehri",  "vegetables", "fiery masalas", "topped with desi ghee", "rice"));
        rc.add(new Recipe(6, "Low fat Dahi Chicken", "chicken", "turmeric", "red chilli", "garlic paste"));
        

        return rc;
    }


    
    
    




}
